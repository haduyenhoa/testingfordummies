//
//  UserManager.swift
//  TestingForDummies
//
//  Created by HA Duyen Hoa on 27.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

class UserManager {
    private let authenticatedKey = "Authenticated"

    static private let shared = UserManager()

    func dummyLogin(_ username: String, password: String) {
        if username.count == 8 {
            UserDefaults.standard.set(true, forKey: authenticatedKey)
        } else {
            UserDefaults.standard.set(false, forKey: authenticatedKey)
        }
    }

    func dummyLogout() {
        UserDefaults.standard.set(false, forKey: authenticatedKey)
    }
}
