//
//  Calculator.swift
//  TestingForDummies
//
//  Created by HA Duyen Hoa on 07.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

class Calculator {
    /**
     A dummy sum function
     - Parameter first: first value
     - Parameter second: second value
     - Returns: first + second
    */
    class func sum(_ first: Int, second: Int) -> Int {
        return first + second
    }

    /**
     A dummy multiple function
     - Parameter first: first value
     - Parameter second: second value
     - Returns: first * second
     */
    class func multiple(_ first: Int, second: Int) -> Int {
        return first * second
    }
}
