//
//  Network.swift
//  TestingForDummies
//
//  Created by WRLD on 14/11/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

class NetworkProvider {
    let urlSession: URLSession
    
    init(_ anUrlSession: URLSession) {
        urlSession = anUrlSession
    }
    
    /**
     Download data from an URL. When the download is finished, call the completion with the eventual downloaded data
     - Parameter completion: completion block to be called when the download task is done.
     - Parameter downloadedData: Downloaded data or nil
    */
    class func getServerConfigData(_ completion: @escaping (_ downloadedData: Data?) -> Void) {
        guard let url = URL(string: "https://google.com") else {
            completion(nil)
            return
        }
        
        //create download task and start to download
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in
            completion(data)
        }
        dataTask.resume()
    }
    
    /**
     DI version of the function 'getServerConfigData' above
     - Parameter urlSession: the URLSession's instance that is used to handle network connections.
     - Parameter completion: completion block to be called when the download task is done.
     - Parameter downloadedData: Downloaded data or nil
     - Parameter error: an error if occurs or nil
     */
    class func getServerConfigData(_ urlSession: URLSession,
                                   completion: @escaping (_ downloadedData: Data?, _ error: Error?) -> Void) {
        guard let url = URL(string: "https://google.com") else {
            completion(nil, NSError(domain: "dummytest.ch", code: 999, userInfo: nil))
            return
        }
        
        //create download task and start to download
        let dataTask = urlSession.dataTask(with: url) { (data, _, error) in
            completion(data, error)
        }
        
        dataTask.resume()
    }
}
