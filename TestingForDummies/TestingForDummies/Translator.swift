//
//  Translator.swift
//  TestingForDummies
//
//  Created by WRLD on 17/11/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

class Translator {
    private static let texts: [String: [String: String]]
        = ["en": ["yes_button": "Yes", "no_button": "No"],
           "fr": ["yes_button": "Oui", "no_button": "Non"]]
    private(set) var currentLangue = "en"
    
    func setUp(_ newLangue: String) {
        currentLangue = newLangue
    }
    
    func message(_ key: String) -> String? {
        return Translator.texts[currentLangue]?[key]
    }
}
