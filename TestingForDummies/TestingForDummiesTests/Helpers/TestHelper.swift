//
//  TestHelper.swift
//  TestingForDummiesTests
//
//  Created by WRLD on 02/12/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

class JSONTestHelper {
    class func jsonData(_ filename: String) -> Data? {
        let bundle = Bundle(for: JSONTestHelper.self)
        
        if let path = bundle.path(forResource: filename, ofType: "json") {
            return try? Data(contentsOf: URL(fileURLWithPath: path))
        }
        
        return nil
    }
}




