//
//  FakeUrlSession.swift
//  TestingForDummiesTests
//
//  Created by WRLD on 01/12/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

enum FakeDataTaskResponse {
    case success(data: Data)
    case failure(error: Error)
    case unauthorized
}

class FakeUrlSession: URLSession {
    private var responseData: Data?
    private var error: Error?
    private var urlResponse: URLResponse?
    
    private var url: URL?
    
    init(_ response: FakeDataTaskResponse) {
        switch response {
        case .success(let data):
            responseData = data
            guard let _url = self.url else {
                return
            }
            urlResponse = HTTPURLResponse(url: _url,
                                          statusCode: 200,
                                          httpVersion: nil,
                                          headerFields: nil)
        case .failure(let error):
            self.error = error
        case .unauthorized:
            guard let _url = self.url else {
                return
            }
            urlResponse = HTTPURLResponse(url: _url,
                                          statusCode: 404,
                                          httpVersion: nil,
                                          headerFields: nil)
        }
    }
    
    override func dataTask(with url: URL,
                           completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
        self.url = url
        return FakeUrlSessionDataTask({
            completionHandler(self.responseData, self.urlResponse, self.error)
        })
    }
}

class FakeUrlSessionDataTask: URLSessionDataTask {
    let completionHandler: () -> Void
    
    init(_ completion: @escaping () -> Void) {
        completionHandler = completion
    }
    override func resume() {
        completionHandler()
    }
}
