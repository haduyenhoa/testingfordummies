//
//  NetworkProviderTests.swift
//  TestingForDummiesTests
//
//  Created by WRLD on 14/11/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import XCTest

@testable import TestingForDummies
class NetworkProviderTests: XCTestCase {

    func testGetConfig() {
        //create the expectation object
        let expectation = XCTestExpectation(description: "Must be able to download the config file")
        
        //execute the async method
        NetworkProvider.getServerConfigData { (downloadedData) in
            guard let data = downloadedData, !data.isEmpty else {
                return
            }
            
            //fulfill if the result of async method match the required behaviour
            expectation.fulfill()
        }
        
        //wait until timeout, if the expectation is fulfilled, test case passes
        wait(for: [expectation], timeout: 30.0)
    }

}

import Quick
import Nimble
import OHHTTPStubs

class NetworkProviderQuickTests: QuickSpec {
    override func spec() {
        describe("a NetworkProvider") {
            //this test may fail if there is no internet connection
            context("when using real connection") {
                var downloadedData: Data?
                it("should be able to get server config data correctly") {
                    NetworkProvider.getServerConfigData({ data in
                        downloadedData = data
                    })
                    
                    expect(downloadedData).toNotEventually(beNil())
                }
            }

            context("when network is up and running") {
                beforeEach {
                    OHHTTPStubs.stubRequests(passingTest: { request -> Bool in
                        //return true if we want to stub that request
                        return true
                    }, withStubResponse: { request -> OHHTTPStubsResponse in
                        //return the desired response (or error)
                        return OHHTTPStubsResponse(data: "ok".data(using: .utf8)!,
                                                   statusCode: 200, headers: nil)
                    })
                }

                var downloadedData: Data?
                it("should be able to get server config data correctly") {
                    NetworkProvider.getServerConfigData({ data in
                        downloadedData = data
                    })

                    expect(downloadedData).toNotEventually(beNil())
                    expect(String(data: downloadedData!, encoding: .utf8)) == "ok"
                }
            }
            
            //MARK: test with subclassing
            context("using my DI NetworkProvider") {
                it("should be able to get server config correctly when server is up and running") {
                    var downloadedData: Data?
                    var networkError: Error?
                    let fakeUrlSession = FakeUrlSession(.success(data: "ok".data(using: .utf8)!))
                    
                    NetworkProvider.getServerConfigData(fakeUrlSession, completion: { (data, error) in
                        downloadedData = data
                        networkError = error
                    })
                    
                    expect(downloadedData).toNotEventually(beNil())
                    expect(String(data: downloadedData!, encoding: .utf8)) == "ok"
                    expect(networkError).to(beNil())
                }
                
                it("should receive error when server is down") {
                    var downloadedData: Data?
                    var networkError: Error?
                    
                    let fakeError = NSError(domain: "dummytest.ch", code: 500, userInfo: nil)
                    let fakeUrlSession = FakeUrlSession(.failure(error: fakeError))
                    
                    NetworkProvider.getServerConfigData(fakeUrlSession, completion: { (data, error) in
                        downloadedData = data
                        networkError = error
                    })
                    
                    expect(downloadedData).toEventually(beNil())
                    expect(networkError).toNot(beNil())
                    expect((networkError as NSError?)?.code) == 500
                    
                }
            }
        }
    }
}
