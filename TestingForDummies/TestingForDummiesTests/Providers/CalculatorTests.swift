//
//  CalculatorTests.swift
//  TestingForDummiesTests
//
//  Created by HA Duyen Hoa on 07.11.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import XCTest

@testable import TestingForDummies
class CalculatorTests: XCTestCase {

    ///This test will fail
    func testFailingTest() {
        let a = 1
        let b = 2
        let c = Calculator.sum(a, second: b)

        //un comment this line to have a failing test
        //XCTAssertEqual(c, 2, "Expect 'sum' of \(a) and \(b) to be 2")
    }

    ///Some assert examples
    func testAsserts() {
        var productNumber: Int?
        XCTAssertNil(productNumber)

        productNumber = 1
        XCTAssertNotNil(productNumber)

        XCTAssertEqual(productNumber, 1, "Product number must be set to 1")
        XCTAssertNotEqual(productNumber, 2)

        XCTAssertTrue(productNumber == 1)
        XCTAssertFalse(productNumber == 2)
    }


}
