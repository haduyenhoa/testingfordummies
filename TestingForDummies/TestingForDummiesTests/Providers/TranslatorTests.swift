//
//  TranslatorTests.swift
//  TestingForDummiesTests
//
//  Created by WRLD on 17/11/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble

@testable import TestingForDummies
class TranslatorTests: QuickSpec {
    override func spec() {
        describe("a Translator") {
            var translator: Translator?
            
            context("being initialized") {
                beforeEach {
                    translator = Translator()
                }
                
                it("should be able to initialize correctly") {
                    expect(translator).notTo(beNil())
                }
                
                it("should use english as default language") {
                    expect(translator?.message("yes_button")) == "Yes"
                }
            }
            
            context("translation to french") {
                beforeEach {
                    translator = Translator()
                    translator?.setUp("fr")
                }
                
                it("should return french text") {
                    let yesButtonTitle = translator?.message("yes_button")
                    expect(yesButtonTitle) == "Oui"
                    
                    let noButtonTitle = translator?.message("no_button")
                    expect(noButtonTitle) == "Non"
                }
            }
        }
        
        describe("given a") {
            context("when b") {
                beforeEach {
                    //prepare context
                }
                
                it("should do/ave c") {
                    expect("1") == "1"
                }
            }
        }
    }

}


import XCTest

class TranslatorXCTests: XCTestCase {

    func testSetupTranslator() {
        let translator = Translator()
        let yesButton = translator.message("yes_button")

        XCTAssertNotNil(yesButton, "Should be able to get a valid localized string")
        XCTAssertEqual(yesButton, "Yes", "Should use english as default language")
    }

    func testMessageInFrench() {
        let translator = Translator()
        translator.setUp("fr")

        let yesButton = translator.message("yes_button")
        let noButton = translator.message("no_button")

        XCTAssertNotNil(yesButton, "Should be able to get a valid localized string")
        XCTAssertNotNil(noButton, "Should be able to get a valid localized string")
        XCTAssertEqual(yesButton, "Oui", "Translation for yesButton in French is 'Oui'")
        XCTAssertEqual(noButton, "Non", "Translation for yesButton in French is 'Non'")
    }

    func test_LoginSuccessWhenUsernameAndPasswordMatch() {

    }
}


